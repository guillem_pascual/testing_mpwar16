<?php

namespace MpwarUnit\Example;

use PHPUnit_Framework_TestCase;
use Mpwar\Example\DummyClass;

final class DummyTest extends PHPUnit_Framework_TestCase
{
    /** @test */
    public function should_pass_using_a_real_class()
    {
        $dummy_class = new DummyClass;
        $this->assertTrue($dummy_class->getTrue());
    }
}
